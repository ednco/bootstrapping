#!/usr/bin/env bash
set -eo pipefail

export DEBIAN_FRONTEND=noninteractive
export APP_REPO=${APP_REPO:-"https://bitbucket.org/ednco/djednco.git"}

export PG_MAJOR=${PG_MAJOR:-"9.3"}
export PGIS_MAJOR=${PGIS_MAJOR:-"2.1"}

if `tty -s`; then
  mesg n
fi

if ! which apt-get &>/dev/null
then
  echo "This installation script requires apt-get. For manual installation instructions."
  exit 1
fi


apt-get update
locale-gen en_US.UTF-8 && update-locale LANG=en_US.UTF-8
apt-get -y upgrade
apt-get install -y git curl make software-properties-common build-essential
apt-get install -y mercurial
apt-get install -y postgresql-${PG_MAJOR}-postgis-${PGIS_MAJOR} postgresql-contrib-${PG_MAJOR} postgresql-server-dev-${PG_MAJOR}
apt-get install -y nginx supervisor memcached
apt-get install -y libjpeg8-dev zlib1g-dev libwebp-dev libopenjpeg-dev

#apt-get install -y git curl software-properties-common build-essential

apt-get install -y python-pip python-dev
pip install virtualenvwrapper

#make install


echo "====================================================="
echo "Done installing packages"
echo "  Now what?"

