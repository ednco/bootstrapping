#!/bin/bash
set -e
# set -x

APP=$1

PG_MAJOR=${PG_MAJOR:-"9.3"}

echo
echo "---> Setting up database \"${APP}\" on $(pg_config --version)"
echo

USER=${APP}
PASS=${PASS:-$(openssl rand -base64 32|base64)}
DB=${APP}

# Connection settings (hardcoded)
PREFIX="postgis"
IP=127.0.0.1
PORT=5432

# POSTGRESQL_TEMPLATE=${POSTGRESQL_TEMPLATE:-"DEFAULT"}

echo "     Creating user: \"${USER}\" with password: \"${PASS}\""
sudo -u postgres createuser --echo --no-password ${USER}
sudo -u postgres psql --echo-all --echo-queries -c "ALTER USER ${USER} WITH PASSWORD '${PASS}';"
echo "     Creating database: \"${DB}\" with owner \"${USER}\""
sudo -u postgres createdb   --echo --encoding=UTF-8 --owner=${USER} ${DB}
echo "     Enabling postGIS for database: \"${DB}\""
sudo -u postgres psql --echo-all --echo-queries --dbname=${DB} -c " \
	CREATE EXTENSION postgis; \
	CREATE EXTENSION postgis_topology;"

echo
echo "----> Database setup complete"
echo
echo "      Connect with the following information:"
echo
echo "          PGHOST=${IP}"
echo "          PGPORT=${PORT}"
echo "          PGUSER=${USER}"
echo "          PASSWD=${PASS}"
echo "          PGDATABASE=${DB}"
echo
echo "          DATABASE_URL=${PREFIX}://${USER}:${PASS}@${IP}:${PORT}/${USER}"
echo
echo
