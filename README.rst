==================================
EDNCO SERVER BOOTSTRAPPING SCRIPTS
==================================

(C)2014 EDNCO Inc. - All rights reserved - See LICENCE file



Prerequisites
-------------

* A server with root access
* Read access to the repository 


Steps
-----
1. Log onto the server as root (or sudoer)::

    ssh root@server
    cd /root

2. Install git::

    apt-get install git 

3. Clone this repository::

    git clone git@bitbucket.org:ednco/bootstrapping.git
    cd bootstrapping
    ./bootstrap.sh