#!/usr/bin/env bash
set -eo pipefail

# sets up a Django configuration on ubuntu vagrant box 
# using virtualenvwrapper

source `which virtualenvwrapper.sh`
mkvirtualenv ednco

cd /vagrant

# install requirements
pip install -r requirements.txt

cd bootstrapping

# Create database
PASS=ednco ./pgstart.sh ednco

# Configure nginx and supervisor
sudo cp etc/nginx/vagrant.conf /etc/nginx/sites-available/

# Configure supervisor
sudo cp etc/supervisor/conf.d/vagrant.conf /etc/supervisor/conf.d/


