# ex: set tabstop=4 noexpandtab:

PG_MAJOR ?= 9.3
PGIS_MAJOR ?= 2.1
APP ?= ednco
APPREPO ?= https://bitbucket.org/ednco/djednco.git
WWW ?= /var/www

VIRTUAL_ENV ?= ${WWW}/${APP}
APPDIR=${WWW}/${APP}/app

USER=${APP}

.PHONY: all install locale base update upgrade environment app db requirements pull  client server pgclient pgserver nginx supervisor foreman clean-environment www user fini help



all:
	# Type "make install" to install.

vagrant: upgrade locale base server

install: upgrade locale base server

base:
	apt-get install -y python-pip python-dev build-essential
	pip install virtualenvwrapper

client: pgclient

server: pgserver nginx supervisor memcached imaging

locale:
	locale-gen en_US.UTF-8 && update-locale LANG=en_US.UTF-8

update:
	apt-get update

upgrade:
	apt-get -y upgrade

clean:
	apt-get clean

www:


user:
	@echo "Creating user"
	adduser --home ${WWW}/${APP} --disabled-password --disabled-login --ingroup www-data --shell /bin/bash ${USER}
	#adduser --home ${WWW}/${APP} --disabled-password --disabled-login --shell /bin/bash ${USER}
	adduser ${USER} www-data

environment:
	@echo "Setting up virtual environment"
	sudo -u ${USER} virtualenv ${WWW}/${APP}
	@echo "    Next: make app"

app:
	sudo -u ${USER} git clone ${APPREPO} ${APPDIR}
	@echo "    Next: make requirements"

requirements:
	sudo -u ${USER} bash -c " \
		source ${VIRTUAL_ENV}/bin/activate \
		&& pip install -r ${APPDIR}/requirements.txt"

pull:
	sudo -u ${USER} bash -c " \
		source ${VIRTUAL_ENV}/bin/activate \
		&& cd ${APPDIR} \
		&& ${USER} git pull"

db:
	./pgstart.sh $(APP)
	@echo
	@echo "   Copy this outpout to your your ENV file "




pgclient:
	#apt-get install -y libpq-dev postgis

pgserver:
	@echo "Installing postgresql with postgis support"
	apt-get install -y postgresql-${PG_MAJOR}-postgis-${PGIS_MAJOR} postgresql-contrib-${PG_MAJOR} postgresql-server-dev-${PG_MAJOR}
	#postgis libpq-dev

nginx:
	@echo "Installing nginx"
	apt-get install -y nginx

supervisor:
	@echo "Installing supervisor"
	apt-get install -y supervisor

foreman:
	echo "deb http://deb.theforeman.org/ trusty 1.5" > /etc/apt/sources.list.d/foreman.list
	echo "deb http://deb.theforeman.org/ trusty 1.5" >> /etc/apt/sources.list.d/foreman.list
	wget -q http://deb.theforeman.org/pubkey.gpg -O- | apt-key add -
	apt-get update && apt-get install -y foreman-installer
	foreman-installer -i

memcached:
	apt-get install -y memcached

imaging:
	apt-get install -y libjpeg8-dev zlib1g-dev libwebp-dev libopenjpeg-dev

fini:
	@echo "c'est fait"


clean-environment:
	# Removes completely the environment:
	#    Delete /var/www
	# 	 Remove nginx/supervisor settings
	#	 Delete database
	# Not implemented:


# count:
#   @echo "Core lines:"
#   @cat dokku bootstrap.sh | wc -l
#   @echo "Plugin lines:"
#   @find plugins -type f | xargs cat | wc -l
#   @echo "Test lines:"
#   @find tests -type f | xargs cat | wc -l

