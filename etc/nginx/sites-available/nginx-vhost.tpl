# Nginx Vhost file for Django as gunicorn reverse proxy and static assets server
# See: http://gunicorn-docs.readthedocs.org/en/latest/deploy.html

upstream ednco {

  # TCP setup
  server 127.0.0.1:10001 fail_timeout=0;

  # UNIX domain socket setups:
  # server unix:/run/ednco_gunicorn.sock fail_timeout=0;
}


###
## catchall
###

server {
  listen * default_server;
  listen 443;
  server_name _;

  ssl_certificate /etc/nginx/ssl/dev.ednco.com/server.crt;
  ssl_certificate_key /etc/nginx/ssl/dev.ednco.com/server.key;

  return 403;
}


###
## http server at port 80 redirects to https
###

server {
  listen 80;
  server_name dev.ednco.com;

  return 301 https://$host$request_uri;
}

# $server_name returns the first element server_name,
# $host redirects all elements in server_name


###
## https server at port 443
###

server {
  server_name dev.ednco.com;
  listen 443 ssl spdy;

  access_log /var/www/ednco/logs/nginx_access.log;
  error_log /var/www//ednco/logs/nginx_error.log;

  # HSTS
  add_header Strict-Transport-Security "max-age=31536000; includeSubdomains";

  ssl on;
  ssl_certificate /etc/nginx/ssl/dev.ednco.com/server.crt;
  ssl_certificate_key /etc/nginx/ssl/dev.ednco.com/server.key;

  ## Use default server-wide settings for protocols
  ssl_session_cache shared:SSL:10m;
  ssl_session_timeout 10m;
  ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
  ssl_prefer_server_ciphers on;
  ssl_ciphers "HIGH:!aNULL:!MD5 or HIGH:!aNULL:!MD5:!3DES";

  # The following is more detailed but apparently not as "good" as the above - IE8 support issues in both
  #ssl_ciphers "EECDH+ECDSA+AESGCM EECDH+aRSA+AESGCM EECDH+ECDSA+SHA384 EECDH+ECDSA+SHA256 EECDH+aRSA+SHA384 EECDH+aRSA+SHA256 EECDH+aRSA+RC4 EECDH EDH+aRSA RC4 !aNULL !eNULL !LOW !3DES !MD5 !EXP !PSK !SRP !DSS !RC4";

  keepalive_timeout 500;
  proxy_read_timeout 300;
  client_max_body_size 20M;

  # path for static files
  root /var/www/ednco/public_html;

  location /static {
      autoindex off;
  }

  location /media {
      autoindex off;
  }

  location / {
    try_files $uri @proxy_to_app;
  }

  location @proxy_to_app {

    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    #proxy_set_header Host $http_host;
    proxy_set_header Host $host;
    proxy_set_header X-Forwarded-Host $server_name;

    # enable this only if you use HTTPS
    proxy_set_header X-Forwarded-Proto https;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_redirect off;

    add_header P3P 'CP="ALL DSP COR PSAa PSDa OUR NOR ONL UNI COM NAV"';
    add_header Front-End-Https on;

    if (!-f $request_filename) {
        proxy_pass http://ednco;
        break;
    }
  }


  # Error pages
  error_page 500 502 503 504 /50x.html;
  location = /50x.html {
    #root /var/www/ednco/public_html;
  }

}