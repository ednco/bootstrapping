user www-data;
worker_processes 4;
pid /run/nginx.pid;

# user nobody nogroup; # for systems with a "nogroup"
# user nobody nobody; # for systems with "nobody" as a group instead

# Feel free to change all paths to suit your needs here, of course
error_log /var/log/nginx.error.log;

events {
  worker_connections 768; # increase if you have lots of clients
  # multi_accept on;
  # accept_mutex on; # "on" if nginx worker_processes > 1
  # use epoll; # enable for Linux 2.6+
  # use kqueue; # enable for FreeBSD, OSX
}


http {

  ##
  # Basic Settings
  ##

  # you generally want to serve static files with nginx since neither
  # Unicorn nor Rainbows! is optimized for it at the moment
  sendfile on;

  tcp_nopush on; # off may be better for *some* Comet/long-poll stuff
  tcp_nodelay off; # on may be better for some Comet/long-poll stuff

  keepalive_timeout 65;
  types_hash_max_size 2048;
  # server_tokens off;

  # server_names_hash_bucket_size 64;
  # server_name_in_redirect off;

  include /etc/nginx/mime.types;
  default_type application/octet-stream;  # fallback

  ##
  # Logging Settings
  ##

  access_log /var/log/nginx/access.log;
  error_log /var/log/nginx/error.log;
  # access_log /var/log/nginx.access.log combined;  # click tracking!


  ##
  # Gzip Settings
  ##

  # we haven't checked to see if Rack::Deflate on the app server is
  # faster or not than doing compression via nginx.  It's easier
  # to configure it all in one place here for static files and also
  # to disable gzip for clients who don't get gzip/deflate right.
  # There are other other gzip settings that may be needed used to deal with
  # bad clients out there, see http://wiki.nginx.org/NginxHttpGzipModule
  gzip on;
  #gzip_disable "msie6";
  gzip_disable "MSIE [1-6]\.";

  # gzip_vary on;
  gzip_proxied any;
  gzip_min_length 500;

  # gzip_comp_level 6;
  # gzip_buffers 16 8k;
  # gzip_http_version 1.1;

  gzip_types text/plain text/css
             text/comma-separated-values
             text/xml application/xml
             application/json text/javascript application/x-javascript
             application/atom+xml application/xml+rss;
  # skipped (always zipped):
  #          text/html text/xml

  ##
  # nginx-naxsi config
  ##
  # Uncomment it if you installed nginx-naxsi
  ##

  #include /etc/nginx/naxsi_core.rules;

  ##
  # nginx-passenger config
  ##
  # Uncomment it if you installed nginx-passenger
  ##

  #passenger_root /usr;
  #passenger_ruby /usr/bin/ruby;

  ##
  # Virtual Host Configs
  ##

  include /etc/nginx/conf.d/*.conf;
  include /etc/nginx/sites-enabled/*;
}

#mail {
# # See sample authentication script at:
# # http://wiki.nginx.org/ImapAuthenticateWithApachePhpScript
#
# # auth_http localhost/auth.php;
# # pop3_capabilities "TOP" "USER";
# # imap_capabilities "IMAP4rev1" "UIDPLUS";
#
# server {
#   listen     localhost:110;
#   protocol   pop3;
#   proxy      on;
# }
#
# server {
#   listen     localhost:143;
#   protocol   imap;
#   proxy      on;
# }
#}


