; EDNCO default supervisor conf file
; copy to /etc/supervisor/conf.d/ednco.conf

;[unix_http_server]
;file = /run/supervisor.sock
;chmod = 0777
;chown= nobody:nogroup
;username = ednco
;password = 123


[program:ednco]
user=ednco

directory=/var/www/ednco/app
command=/var/www/ednco/bin/run_gunicorn --pythonpath /var/www/ednco/app --timeout=300 --workers=1 --bind 127.0.0.1:10001 ednco.wsgi:application
; --reload --log-level debug --log-file -

autostart=true
autorestart=true
redirect_stderr=True
stdout_logfile=/var/www/ednco/logs/gunicorn.log
stdout_logfile_maxbytes=2MB


;environment =
;   DJANGO_DEBUG="True",
;   PYTHONPATH="/var/www/ednco/app",

; IMPORTANT: Do not use "%" character in env variables (such as DJANGO_SECRET_KEY)

; Note: Environment variables at the very end since overall they can't be used in configuration
; Note: Possibly starting in 3.1 will come the ability to specify command line like so:
; command=/var/www/ednco/bin/gunicorn --pythonpath %(ENV_PYTHONPATH)s --timeout=300 --workers=1 --bind 127.0.0.1:10001 ednco.wsgi:application
